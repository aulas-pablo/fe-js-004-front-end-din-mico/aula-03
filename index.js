import fs from "fs"


function getProducts() {
    return new Promise((resolve, reject) => {
        const products = [
            {
                id: 1,
                name: "produto 1"
            },
            {
                id: 2,
                name: "produto 2"
            }
        ]

        if (products.length > 0) {
            resolve(products)
        }
        const error = new Error("NAO POSSUI PRODUTOS")
        reject(error)


    })
}

function getPrices(id) {
    return new Promise((resolve, reject) => {
        const prices = [
            {
                productId: 1,
                price: 10
            },
            {
                productId: 2,
                price: 500
            }
        ]

        const price = prices.find(price => price.productId == id)
        resolve(price)
        if (price) {
            resolve(price)
        }
        const error = new Error("PREÇO NAO ENCONTRADO")
        reject(error)

    })

}


//SOLUCAO BASEADO NO ASYNC AWAIT
async function showProducts() {
    try {
        const products = await getProducts()
        console.log(products)
        const price = await showPrices(products[0])
        console.log(`O PRECO DO MEU PRODUTO ${products[0].name} É R$${price.price},00`)
    } catch (error) {
        console.log(error.message)
    }

}

//SOLUCAO BASEADO NO ASYNC AWAIT
async function showPrices(product) {
    const price = await getPrices(product.id)
    if (!price) throw new Error("PREÇOS NAO LOCALIZADOS")
}

showProducts()




//SOLUÇAO CALLBACK
getProducts()
    .then((products) => {
        getPrices(products[0].id)
            .then(({ price }) => console.log(`O PRECO DO MEU PRODUTO ${products[0].name} É R$${price},00`))
            .catch((erro) => console.log(erro))

    })
    .catch((erro) => console.log(erro))
    .finally(() => console.log("finalizou"))




